def student(name,course):
    return'{} is {} student'.format(name,course)

s1=student('Aneeta','Python')
s2=student('Meenu','java')
print(s1)
print(s2)

##Get DocString of a function
print(student._doc_)
print(len._doc_)
